const { GraphQLServer } = require("graphql-yoga");

let posts = [
  {
    id: "1",
    title: "Part3: Single Mutation GraphQL API",
    content: "Simple create mutation for GraphQL ...",
  },
  {
    id: "2",
    title: "Test Mutaion",
    content: "Test Mutaion create Post",
  },
];

let postId = posts.length + 1;

const typeDefs = `
type Query {
  info: String!
  hello(name: String): String!
  posts: [Post]
}
type Mutation {
 createNewPost(title: String!, content: String!): Post!
}
  
type Post {
  id: ID!
  title: String!
  content: String!
}
`;

const resolvers = {
  Query: {
    info: () => `This is the API of a Forum GraphQL`,
    hello: (_, { name }) => `Hello ${name || "World"}`,
    posts: () => posts,
  },
  Mutation: {
    createNewPost: (parent, args) => {
      const newPost = {
        id: `${postId++}`,
        title: args.title,
        content: args.content,
      };

      posts.push(newPost);
      return newPost;
    },
  },
};

const server = new GraphQLServer({
  typeDefs,
  resolvers,
});
server.start(() => console.log(`Server is running on http://localhost:4000`));
